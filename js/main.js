/*
Created by Lenovo on 24.05.2016.*/
var swiper = new Swiper('.swiper .swiper-container', {
    pagination: ' .swiper-pagination',
    slidesPerView: 1,
    slidesPerGroup: 1,
    paginationClickable: true,
    nextButton: '.swiper-button-next',
    prevButton: '.swiper-button-prev'
});

var head_swiper = new Swiper('header .swiper-container', {
    pagination: ' .swiper-pagination',
    slidesPerView: 1,
    slidesPerGroup: 1,
    paginationClickable: true

});
/*


var back =[
    "#ca0b03",
    "#0272a2",
    "#88cada",
    "#d7c703",
    "#9fc377",
    "#21a4a2",
    "#68a728",
    "#cd3d8b",
    "#87D37C",
    "#4ECDC4",
    "#AF1BE8",
    "#FF915D",
    "#FFC043",
    "#D13939"];
function ref() {

    // First random color
    var rand1 = back[Math.floor(Math.random() * back.length)];
    // Second random color
    var rand2 = back[Math.floor(Math.random() * back.length)];

    var grad = $('header');

    // Convert Hex color to RGB
    function convertHex(hex,opacity){
        hex = hex.replace('#','');
        r = parseInt(hex.substring(0,2), 16);
        g = parseInt(hex.substring(2,4), 16);
        b = parseInt(hex.substring(4,6), 16);

        // Add Opacity to RGB to obtain RGBA
        result = 'rgba('+r+','+g+','+b+','+opacity/100+')';
        return result;
    };
    // Gradient rules
    grad.css('background-color', convertHex(rand1,90) );
    grad.css("background-image", "-webkit-gradient(linear, left top, left bottom, color-stop(0%,"+ convertHex(rand1,90) +"), color-stop(100%,"+ convertHex(rand2,40) +"))");
    grad.css("background-image", "-webkit-linear-gradient(top,  "+ convertHex(rand1,90) +" 0%,"+ convertHex(rand2,90) +" 100%)");
    grad.css("background-image", "-o-linear-gradient(top, "+ convertHex(rand1,90) +" 0%,"+ convertHex(rand2,90) +" 100%)");
    grad.css("background-image", "-ms-linear-gradient(top, "+ convertHex(rand1,90) +" 0%,"+ convertHex(rand2,90) +" 100%)");
    grad.css("background-image", "linear-gradient(to bottom, "+ convertHex(rand1,90) +" 0%,"+ convertHex(rand2,90) +" 100%)");
    grad.css("filter", "progid:DXImageTransform.Microsoft.gradient( startColorstr='"+ convertHex(rand1,90) +"', endColorstr='"+ convertHex(rand2,90) +"',GradientType=0 )");
    grad.css("transition","1.5s").css("-webkit-transition","1.5s").css("-o-transition","1.5s").css("-moz-transition","1.5s");
};
setInterval(ref, 5000);


*/




var colors = new Array(
    [62,35,255],
    [60,255,60],
    [255,35,98],
    [45,175,230],
    [255,0,255],
    [255,128,0]);

var step = 0;
//color table indices for:
// current color left
// next color left
// current color right
// next color right
var colorIndices = [0,1,2,3];

//transition speed
var gradientSpeed = 0.002;

function updateGradient()
{

    if ( $===undefined ) return;

    var c0_0 = colors[colorIndices[0]];
    var c0_1 = colors[colorIndices[1]];
    var c1_0 = colors[colorIndices[2]];
    var c1_1 = colors[colorIndices[3]];

    var istep = 1 - step;
    var r1 = Math.round(istep * c0_0[0] + step * c0_1[0]);
    var g1 = Math.round(istep * c0_0[1] + step * c0_1[1]);
    var b1 = Math.round(istep * c0_0[2] + step * c0_1[2]);
    var color1 = "rgb("+r1+","+g1+","+b1+")";

    var r2 = Math.round(istep * c1_0[0] + step * c1_1[0]);
    var g2 = Math.round(istep * c1_0[1] + step * c1_1[1]);
    var b2 = Math.round(istep * c1_0[2] + step * c1_1[2]);
    var color2 = "rgb("+r2+","+g2+","+b2+")";

    $('header').css({
        background: "linear-gradient(to bottom, "+color1+" 0%, "+color2+" 100%)"}).css({
        background: "linear-gradient(to bottom, "+color1+" 0%, "+color2+" 100%)"});

 $('.macbook>path').css({
fill: color1 });
 $('.watch>polyline').css({
 fill: color1 });
 $('.iphone>polyline').css({
 fill: color2 });

    step += gradientSpeed;
   if ( step >= 1 )
    {
        step %= 1;
        colorIndices[0] = colorIndices[1];
        colorIndices[2] = colorIndices[3];

        //pick two new target color indices
        //do not pick the same as the current one
        colorIndices[1] = ( colorIndices[1] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;
        colorIndices[3] = ( colorIndices[3] + Math.floor( 1 + Math.random() * (colors.length - 1))) % colors.length;

    }
}

setInterval(updateGradient,10);


$(".swiper-button-prev").on("click", function() {
    $(this).addClass('swing-animation').one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd", function(){
        $(this).removeClass('swing-animation');
    });
});
$(".swiper-button-next").on("click", function() {
    $(this).addClass('swing-animation').one("webkitAnimationEnd mozAnimationEnd MSAnimationEnd", function(){
        $(this).removeClass('swing-animation');
    });
});
